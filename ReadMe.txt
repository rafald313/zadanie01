
Uprzejmie proszę o napisanie aplikacji, której zadaniem jest łączenie dwóch danych pochodzących z różnych źródeł.

Danymi są LICZBY, a jako ich źródła można przyjąć przykładowo:
- generowanie liczb losowych języka Java,
- www.random.org (API REST),
- odczyt z bazy danych (hipotetyczna zewnętrzna baza lub baza pamięciową).


Natomiast sposób łączenia to DODAWANIE.
Należy przyjąć, tak jak ma to miejsce podczas codziennej pracy, iż
aplikacja będzie rozwijana i wymagania mogą się zmienić (np. inny typ
danych, sposób łączenia). Architektura rozwiązania musi być zatem
elastyczna i podatna na zmiany.

Założenia:
1) Aplikacja może wykorzystać dowolny framework. Może być również napisanie w czystej Javie.
2) Kod musi być możliwy do skompilowania i uruchomienia na komputerze z zainstalowanym JDK w wersjach 8 i 11, Mavenem oraz Gradle.
3) Kod musi być przekazany w formie archiwum lub jako link do otwartego repozytorium. W obu przypadkach powinien spełniać wymogi projektu produkcyjnego.