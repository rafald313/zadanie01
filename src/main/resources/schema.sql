CREATE TABLE random_numbers (
    id   INTEGER      NOT NULL AUTO_INCREMENT,
    number NUMBER NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE results_table (
    id   INTEGER      NOT NULL AUTO_INCREMENT,
    first_value NUMBER NOT NULL,
    second_value NUMBER NOT NULL,
    sum NUMBER NOT NULL,
    PRIMARY KEY (id)
);
