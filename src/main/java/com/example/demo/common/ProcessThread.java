package com.example.demo.common;

import com.example.demo.strategy.Generator;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
@Scope("prototype")
public class ProcessThread implements Runnable {

    private final List<Generator> generators;
    private static final Logger LOGGER = LoggerFactory.getLogger(ProcessThread.class);

    @Override
    public void run() {
        List<Integer> list = new ArrayList<>();
        while (true) {
            try {
                GeneratorTypes generatorTypes = GeneratorTypes.randomGeneratorType();
                for (Generator generator : generators) {
                    if (generator.isValid(generatorTypes)) {
                        int perform = generator.perform();
                        if (list.size() >= 2) {
                            int sum = list.get(0) + list.get(1);
                            LOGGER.info("SUMA : " + sum);
                            list.clear();
                        }
                        LOGGER.info(generatorTypes + " Wartość otrzymana : " + perform);
                        list.add(perform);
                    }
                }
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                LOGGER.error(e.getMessage());
            }
        }
    }
}