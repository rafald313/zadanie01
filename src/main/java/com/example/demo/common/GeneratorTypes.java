package com.example.demo.common;

import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Random;

@RequiredArgsConstructor
public enum GeneratorTypes {
    GENERATOR_JAVA,
    GENERATOR_ORG,
    GENERATOR_DB;

    private static final List<GeneratorTypes> VALUES = List.of(values());
    private static final int SIZE = VALUES.size();
    private static final Random RANDOM = new Random();

    public static GeneratorTypes randomGeneratorType() {
        return VALUES.get(RANDOM.nextInt(SIZE));
    }
}
