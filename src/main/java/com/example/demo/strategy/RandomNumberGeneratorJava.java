package com.example.demo.strategy;

import com.example.demo.common.GeneratorTypes;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
@RequiredArgsConstructor
public class RandomNumberGeneratorJava implements Generator {

    @Override
    public boolean isValid(GeneratorTypes generatorTypes) {
        return GeneratorTypes.GENERATOR_JAVA.equals(generatorTypes);
    }

    @Override
    public int perform() {
        return new Random().nextInt(50);
    }
}
