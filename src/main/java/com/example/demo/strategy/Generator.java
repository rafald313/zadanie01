package com.example.demo.strategy;


import com.example.demo.common.GeneratorTypes;

/**
 * Class: Generator - interfejs obsługujący strategię dla generatora liczb
 */
public interface Generator {
    boolean isValid(GeneratorTypes generatorTypes);

    int perform();
}
