package com.example.demo.strategy;

import com.example.demo.common.GeneratorTypes;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Random;

@Component
@RequiredArgsConstructor
public class RandomNumberGeneratorOrg implements Generator {

    public static final String URL_RANDOME_ORG = "https://www.random.org/integers/?num=1&min=1&max=6&col=1&base=10&format=plain";

    @Override
    public boolean isValid(GeneratorTypes generatorTypes) {
        return GeneratorTypes.GENERATOR_ORG.equals(generatorTypes);
    }

    @Override
    public int perform() {
        int resultNumber;
        try {
            RestTemplate restTemplate = new RestTemplate();
            resultNumber = Integer.parseInt(restTemplate.getForObject(URL_RANDOME_ORG, String.class).trim());
        } catch (Exception e) {
            return new Random().nextInt(50);
        }
        return resultNumber;
    }
}
