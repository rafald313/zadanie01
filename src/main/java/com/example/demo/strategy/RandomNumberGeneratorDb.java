package com.example.demo.strategy;

import com.example.demo.common.GeneratorTypes;
import com.example.demo.model.randomNumber.RandomNumberService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor

public class RandomNumberGeneratorDb implements Generator {

    private final RandomNumberService randomNumberService;

    @Override
    public boolean isValid(GeneratorTypes generatorTypes) {
        return GeneratorTypes.GENERATOR_DB.equals(generatorTypes);
    }

    @Override
    public int perform() {
        return randomNumberService.getRandomNumber();
    }
}
