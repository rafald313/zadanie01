package com.example.demo.model.randomNumber;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "random_numbers")
public class RandomNumber {

    @Id
    @GeneratedValue
    private long id;

    @NonNull
    private long number;
}
