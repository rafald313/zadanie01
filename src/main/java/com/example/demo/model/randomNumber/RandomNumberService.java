package com.example.demo.model.randomNumber;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.Random;

/**
 * <pre>
 * Class: RandomNumberService - serwis do obsługi pobierania liczb z bazy H2
 * </pre>
 */
@Service
@Transactional
@RequiredArgsConstructor
public class RandomNumberService {

    private final RandomNumberRepository randomNumberRepository;

    public int getRandomNumber() {
        long id = new Random().nextInt(10);
        Optional<RandomNumber> numberAtDb = randomNumberRepository.findById(id);
        return numberAtDb.map(randomNumber -> (int) randomNumber.getNumber()).orElseGet(() -> new Random().nextInt(50));
    }
}

